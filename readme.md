# Readme

## Building
Compile this project with Gradle.

## Running
Run the RestServiceApplication main class.

Invoking the aggregation service

Story 1
```
GET http://localhost:8080/aggregation1?pricing=NL,CN,BR,DE&track=109347263,123456891&shipments=109347263

```

Story 2
```
GET http://localhost:8080/aggregation2?pricing=NL,CN,BR,DE,FR&track=109347263,123456891,1,2,3&shipments=109347263,1,2,3,4
```

Story 3
```
GET http://localhost:8080/aggregation3?pricing=NL,CN,BR,DE,BE&track=109347263,123456891,1,2,3&shipments=109347263,1,2,3,4
GET http://localhost:8080/aggregation3?pricing=NL,BE&track=109347263,123456891,1,2,3&shipments=109347263,1,2,3,4
GET http://localhost:8080/aggregation3?pricing=NL,CN,BR,DE,BE&track=109347263,123456891&shipments=109347263
```
