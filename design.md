# Readme

I have chosen to implement this assignment with Spring Boot, since it makes it easy to package and run REST services.

* The backing services (pricing, track, and shipments) have an SLA of 5 seconds.
* If a backing service fails, or does not respond enough, a null value should be returned in the aggregate response.
* The aggregate service has an SLA of 10 seconds - only relevant in story 3.

## Setup stub services
In order to test the aggretation service, we need easy access to the underlying services for pricing, track, and shipments.
An easy solution is to implement these in stub services that return valid responses in the same Spring Boot application.

## Story 1 - aggregate 3 services into one
The aggregate service receives 3 query parameters:
* pricing: list of country codes
* track: list of order numbers
* shipments: list of order numbers

Call all three backing services with the corresponding query parameter and return result into response of aggregation service.

This is implemented with three Spring components that call the backing service, in case of exception returns null.
It will directly call the backing service with the received query parameters.

Since this will call all three backing services sequentially, which have a maximum response time of 5 seconds, the response time of
the aggregation service may be up to 15 seconds.

See ShippingAggregateService1.java

## Story 2 - consolidate requests and call backing service with combined query parameters
The aggregation service will collect requests for each backing service and combine query parameters to minimize traffic to
the backing service.

Since the aggregation service requests are processed in multiple threads, the code that calls the backing service should
be decoupled from the aggregation service. The solution is a queueing pipeline, where the aggregation service puts parameter
values into the queue of the backing service and receives the result asynchronously.

The solution makes use of Future objects:
* the aggregation service makes requests for every backing service:
    * split up the query parameter into single values (order numbers and country codes)
    * send the parameter value into the queue and receive a Future object
* the queue will, for each backing service:
    * wait until it has received 5 parameter values
    * call the backing service with the combined list of parameter values
    * return back in the Future objects the corresponding result
* receive the response for each backing service:
    * get the value from the Future object (this may block until it is actually retrieved)
    * put response in aggregation response
  


## Story 3 - invoke backing service after timeout
In case that the queue does not contain enough items to trigger an invocation of the backing service with a combined list
of query parameters, the aggregation service must, within the requirements of the SLA invoke the backing service with
the items that are in the queue.

The queue simply waits for new items that are added to it and after receiving 5 items it will invoke the backing service -
it now needs to keep track of the oldest item in the queue and make sure that it will be processed at least within 5
seconds.

The item in the queue will use an expiration time - this is the latest that this item must be processed. The QueueConsumer3
uses this expiration time to calculate a timeout for the queue.poll() method.

In the story 2 solution, the backing service is called in the same thread as the QueueConsumer3 class. This waits until
the backing service responds and does not process new items coming into the queue. If many requests come in through the
aggregation service, the queue will grow, and may cause much longer delays than the SLA of the aggregation service of 10
seconds.

To make sure that the aggregation service can fulfill the 10 second response time, the QueueConsumer3 class must call the
backing service in parallel. It uses the ReceivedItemsProcessor to handle the backing service calls and return results
to the Future objects.
