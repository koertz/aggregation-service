package nl.zencode.restservice.story3;

import nl.zencode.restservice.RestServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestServiceApplication.class)
public class SpringContextTest {

  @Test
  public void whenSpringContextIsBootstrapped_thenNoExceptions() {
  }
}