package nl.zencode.restservice.story3;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * @author Koert Zeilstra
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:Feature")
public class RunCucumberTest extends SpringIntegrationTest {
}
