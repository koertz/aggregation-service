package nl.zencode.restservice.story3;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.http.HttpStatus;

/**
 * @author Koert Zeilstra
 */
public class StepDefsIntegrationTest extends SpringIntegrationTest {

  @When("^the client calls /baeldung$")
  public void the_client_issues_POST_hello() throws Throwable {
    executePost();
  }

  @Given("^the client calls /hello$")
  public void the_client_issues_GET_hello() throws Throwable {
    executeGet("http://localhost:8080/hello");
  }

  @When("^the client calls /version$")
  public void the_client_issues_GET_version() throws Throwable {
    executeGet("http://localhost:8080/version");
  }

  @When("^the client calls /aggregation1?(.+)$")
  public void aggregation1(String query) throws Throwable {
    executeGet("http://localhost:8080/aggregation1?" + query);
  }

  @Then("^the client receives status code of (\\d+)$")
  public void the_client_receives_status_code_of(int statusCode) throws Throwable {
    final HttpStatus currentStatusCode = latestResponse.getTheResponse().getStatusCode();
    assertThat("status code is incorrect : " + latestResponse.getBody(), currentStatusCode.value(), is(statusCode));
  }

  @And("^the client receives server version (.+)$")
  public void the_client_receives_server_version_body(String version) {
    assertThat(latestResponse.getBody(), is(version));
  }

  @And("^the response contains \"(.+)\"$")
  public void responseContains(String contents) {
    assertThat(latestResponse.getBody(), containsString(contents));
  }
}