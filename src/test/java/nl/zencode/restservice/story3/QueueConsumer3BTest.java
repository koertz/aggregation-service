package nl.zencode.restservice.story3;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import nl.zencode.restservice.common.BackingServiceClient;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Unit test QueueConsumer3.
 * @author Koert Zeilstra
 */
@ExtendWith(MockitoExtension.class)
class QueueConsumer3BTest {

  static final String KEY = "123";
  static final Double VALUE = 12.34;

  QueueConsumer3<Double> queueConsumer;

  @Mock ExecutorService executorService;
  @Mock BlockingQueue<DeferredRestCall3<Double>> queue = new LinkedBlockingDeque<>();
  @Mock BackingServiceClient<Double> backingServiceClient;
  
  @Mock DeferredRestCall3<Double> deferredRestCall;

  @BeforeEach
  void setUp() {
    queueConsumer = new QueueConsumer3<>(executorService, queue, backingServiceClient);
  }

  @Test
  @DisplayName("run")
  void run() throws InterruptedException {
    
    Mockito.when(queue.poll(anyLong(), eq(TimeUnit.MILLISECONDS))).thenReturn(deferredRestCall).thenReturn(null);
    Mockito.doAnswer(invocation -> {
      deferredRestCall.complete(VALUE);
      return null;
    }).when(executorService).execute(any());
    Map<String, Double> combinedResponse = new HashMap<>();
    combinedResponse.put(KEY, VALUE);
    Mockito.when(backingServiceClient.get(KEY)).thenReturn(combinedResponse);

    ExecutorService queueExecutorService = Executors.newFixedThreadPool(1);
    queueExecutorService.execute(queueConsumer);

    Double result = deferredRestCall.getFuture().join();
    Assertions.assertThat(result).isEqualTo(VALUE);
  }
}