package nl.zencode.restservice.story3;

import java.util.Map;
import java.util.concurrent.Future;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

/**
 * Unit test PricingClient3.
 * @author Koert Zeilstra
 */
@ExtendWith(MockitoExtension.class)
class PricingClient3Test {

  @InjectMocks
  PricingClient3 client;

  @Mock
  RestTemplate restTemplate;

  @Nested
  @DisplayName("get")
  class Get {

    @ParameterizedTest
    @CsvSource(value = {":0", "123:1", "123,456:2"}, delimiter = ':')
    void param(String parameterValue, int expectedSize) {

      Map<String, Future<Double>> responses = client.get(parameterValue);

      Assertions.assertThat(responses.size()).isEqualTo(expectedSize);
    }

    @Test
    @DisplayName("empty")
    void EmptyParam() {

      Map<String, Future<Double>> responses = client.get("");

      Assertions.assertThat(responses.size()).isEqualTo(0);
    }

    @Test
    @DisplayName("single")
    void singleParam() {

      Map<String, Future<Double>> responses = client.get("123");

      Assertions.assertThat(responses.size()).isEqualTo(1);
      Future<Double> response123 = responses.get("123");
      Assertions.assertThat(response123).isNotNull();
      Assertions.assertThat(response123).isNotDone();
    }


    @Test
    @DisplayName("double")
    void doubleParam() {

      Map<String, Future<Double>> responses = client.get("123,456");

      Assertions.assertThat(responses.size()).isEqualTo(2);
      Future<Double> response123 = responses.get("123");
      Assertions.assertThat(response123).isNotNull();
      Assertions.assertThat(response123).isNotDone();
    }

  }
}