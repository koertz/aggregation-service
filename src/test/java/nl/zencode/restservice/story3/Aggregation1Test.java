package nl.zencode.restservice.story3;

import java.util.ArrayList;
import java.util.List;
import nl.zencode.restservice.RestServiceApplication;
import org.approvaltests.combinations.CombinationApprovals;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

/**
 * @author Koert Zeilstra
 */
@SpringBootTest(classes = RestServiceApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class Aggregation1Test {

  public static final String[] PRICING = {null, "NL,CN,BR,DE", "BE,FR"};
  public static final String[] TRACK = {"109347263,123456891", "123"};
  public static final String[] SHIPMENTS = {"109347263", "1,2,3"};


  @LocalServerPort
  private int port;

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void version() {
    Assertions.assertThat(this.restTemplate.getForObject("http://localhost:8080/version",
        String.class)).contains("1.0");
  }

  @Test
  public void aggregation1Approval() {
    CombinationApprovals.verifyAllCombinations(this::callAggregation1, PRICING, TRACK, SHIPMENTS);

  }
  
  private String callAggregation1(String pricing, String track, String shipments) {
    List<String> queryArguments = new ArrayList<>();
    if (pricing != null) {
      queryArguments.add("pricing=" + pricing);
    }
    if (track != null) {
      queryArguments.add("track=" + track);
    }
    if (shipments != null) {
      queryArguments.add("shipments=" + shipments);
    }
    String url = "http://localhost:8080/aggregation1";
    if (queryArguments.size() > 0) {
      url = url + "?" + String.join("&", queryArguments);
    }
    return this.restTemplate.getForObject(url, String.class);
  }

}
