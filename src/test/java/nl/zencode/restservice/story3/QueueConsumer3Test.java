package nl.zencode.restservice.story3;

import static org.mockito.ArgumentMatchers.anyString;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import nl.zencode.restservice.common.BackingServiceClient;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Unit test QueueConsumer3.
 * @author Koert Zeilstra
 */
@ExtendWith(MockitoExtension.class)
class QueueConsumer3Test {

  static final String[] KEY = {"123", "1", "2", "3", "4"};
  static final Double[] VALUE = {12.34, 1.0, 2.0, 3.0, 4.0};

  QueueConsumer3<Double> queueConsumer;

  ExecutorService executorService;
  BlockingQueue<DeferredRestCall3<Double>> queue = new LinkedBlockingDeque<>();
  @Mock BackingServiceClient<Double> backingServiceClient;
  
  DeferredRestCall3<Double> deferredRestCall0;
  DeferredRestCall3<Double> deferredRestCall1;
  DeferredRestCall3<Double> deferredRestCall2;
  DeferredRestCall3<Double> deferredRestCall3;
  DeferredRestCall3<Double> deferredRestCall4;

  @BeforeEach
  void setUp() {
    executorService = Executors.newFixedThreadPool(5);
    deferredRestCall0 = new DeferredRestCall3<>(KEY[0], Long.MAX_VALUE);
    deferredRestCall1 = new DeferredRestCall3<>(KEY[1], Long.MAX_VALUE);
    deferredRestCall2 = new DeferredRestCall3<>(KEY[2], Long.MAX_VALUE);
    deferredRestCall3 = new DeferredRestCall3<>(KEY[3], Long.MAX_VALUE);
    deferredRestCall4 = new DeferredRestCall3<>(KEY[4], Long.MAX_VALUE);
    queueConsumer = new QueueConsumer3<>(executorService, queue, backingServiceClient);
  }

  @Test
  @DisplayName("run")
  void run() throws InterruptedException {
    
    queue.put(deferredRestCall0);
    queue.put(deferredRestCall1);
    queue.put(deferredRestCall2);
    queue.put(deferredRestCall3);
    queue.put(deferredRestCall4);

    Map<String, Double> combinedResponse = new HashMap<>();
    for (int i=0; i<5; i++) {
      combinedResponse.put(KEY[i], VALUE[i]);
    }
    Mockito.when(backingServiceClient.get(anyString())).thenReturn(combinedResponse);

    executorService.execute(queueConsumer);

    Double result = deferredRestCall0.getFuture().join();
    Assertions.assertThat(result).isEqualTo(VALUE[0]);
  }
}