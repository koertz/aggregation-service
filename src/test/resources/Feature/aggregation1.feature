Feature: aggregation1 can be retrieved
  Scenario: client makes call to GET /aggregation1
    When the client calls /aggregation1?pricing=NL,CN,BR,DE&track=109347263,123456891&shipments=109347263
    Then the client receives status code of 200
    And the response contains "109347263"
    