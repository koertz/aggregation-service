package nl.zencode.restservice.story2;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import javax.annotation.PostConstruct;
import nl.zencode.restservice.aggregation.ServiceException;
import nl.zencode.restservice.common.BackingServiceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.ApplicationScope;

/**
 * Client for track backing service.
 * @author Koert Zeilstra
 */
@Component
@ApplicationScope
public class TrackClient2 {
  
  private static final String URL = "http://localhost:8080/track?q=";
  private static final Logger log = LoggerFactory.getLogger(TrackClient2.class);

  @Autowired private RestTemplate restTemplate;
  private ExecutorService executorService = Executors.newCachedThreadPool();
  private BlockingQueue<DeferredRestCall2<String>> queue = new LinkedBlockingDeque<>();
  private BackingServiceClient<String> backingServiceClient;

  @PostConstruct
  public void initialize() {
    backingServiceClient = new BackingServiceClient<>(URL, restTemplate);
    executorService.execute(new QueueConsumer2<>(queue, backingServiceClient));
  }

  /**
   * Call backing service.
   * @param parameterValue Parameter value for service, may contain multiple comma separated values.
   * @return Service response.
   */
  public Map<String, Future<String>> get(String parameterValue) {
    Map<String, Future<String>> futureResponse = new HashMap<>();
    if (parameterValue != null && !parameterValue.isEmpty()) {
      for (String value : parameterValue.split(",")) {
        try {
          DeferredRestCall2<String> deferredCall = new DeferredRestCall2<>(value);
          queue.put(deferredCall);
          futureResponse.put(value, deferredCall.getFuture());
        } catch (InterruptedException e) {
          throw new ServiceException("Failed to insert into queue", e);
        }
      }
    }

    return futureResponse;
  }


}
