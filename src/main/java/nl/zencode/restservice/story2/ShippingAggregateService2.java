package nl.zencode.restservice.story2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import nl.zencode.restservice.common.ShippingAggregateResponse;
import nl.zencode.restservice.aggregation.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Service that combines several other rest service into one aggregate service.
 * @author Koert Zeilstra
 */
@RestController
public class ShippingAggregateService2 {

  @Autowired RestTemplate restTemplate;
  @Autowired private PricingClient2 pricingClient;
  @Autowired private TrackClient2 trackClient;
  @Autowired private ShipmentsClient2 shipmentsClient;


  /**
   * REST service /aggregation2 - aggregates multiple calls into one response.
   * @param pricing Parameter values for pricing service.
   * @param track Parameter values for track service.
   * @param shipments Parameter values for shipments service.
   * @return Aggregated response.
   */
  @GetMapping("/aggregation2")
  public ShippingAggregateResponse aggregation(@RequestParam(value = "pricing", required = false) String pricing, 
      @RequestParam(value = "track", required = false) String track, 
      @RequestParam(value = "shipments", required = false) String shipments) {

    Map<String, Future<Double>> pricingFutureResponse = pricingClient.get(pricing);
    Map<String, Future<String>> trackFutureResponse = trackClient.get(track);
    Map<String, Future<List<String>>> shipmentsFutureResponse = shipmentsClient.get(shipments);

    Map<String, Double> pricingResponse = resolveFutureResponses(pricingFutureResponse);
    Map<String, String> trackResponse = resolveFutureResponses(trackFutureResponse);
    Map<String, List<String>> shipmentsResponse = resolveFutureResponses(shipmentsFutureResponse);

    return new ShippingAggregateResponse(pricingResponse, trackResponse, shipmentsResponse);
  }

  /**
   * Resolve all future responses.
   * @param futureResponses Map with future responses, the key corresponds to the parameterValue.
   * @param <T> Type of returned response in map.
   * @return Map with resolved responses, the key corresponds to the parameterValue.
   */
  private <T> Map<String, T> resolveFutureResponses(Map<String, Future<T>> futureResponses) {
    Map<String, T> resolvedResponse = new HashMap<>();
    try {
      for (String key : futureResponses.keySet()) {
        resolvedResponse.put(key, futureResponses.get(key).get());
      }
    } catch (InterruptedException| ExecutionException e) {
      throw new ServiceException("Failed to resolve Future value", e);
    }
    if (resolvedResponse.size() == 0) {
      resolvedResponse = null;
    }
    return resolvedResponse;
  }

}
