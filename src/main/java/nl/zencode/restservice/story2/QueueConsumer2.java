package nl.zencode.restservice.story2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;
import nl.zencode.restservice.aggregation.ServiceException;
import nl.zencode.restservice.common.BackingServiceClient;

/**
 * Queue for service calls to backing services.
 * This runs continously to handle calls asynchronously.
 * It collects single call parameters until it has received maximumQueueSize number of values; then it will combine
 * these values into a comma separated list and calls the backing service with that list of values.
 * @author Koert Zeilstra
 */
class QueueConsumer2<T> implements Runnable {

  private BlockingQueue<DeferredRestCall2<T>> queue;
  private BackingServiceClient<T> backingServiceClient;

  private boolean running = true;
  private List<DeferredRestCall2<T>> receivedItems = new ArrayList<>();
  private int maximumQueueSize = 5;

  public QueueConsumer2(BlockingQueue<DeferredRestCall2<T>> queue, BackingServiceClient<T> backingServiceClient) {
    this.queue = queue;
    this.backingServiceClient = backingServiceClient;
  }

  @Override
  public void run() {
    while (running) {
      try {
        DeferredRestCall2<T> deferredRestCall = queue.take();
        receivedItems.add(deferredRestCall);
        if (receivedItems.size() >= maximumQueueSize) {
          String combinedParameters = receivedItems.stream().map(restCall -> restCall.getParameterValue())
              .collect(Collectors.joining(","));
          Map<String, T> combinedResponse = backingServiceClient.get(combinedParameters);
          if (combinedResponse == null) {
            for (DeferredRestCall2<T> receivedItem : receivedItems) {
              receivedItem.complete(null);
            }
          } else {
            for (DeferredRestCall2<T> receivedItem : receivedItems) {
              receivedItem.complete(combinedResponse.get(receivedItem.getParameterValue()));
            }
          }
          receivedItems.clear();
        }
      } catch (InterruptedException e) {
        throw new ServiceException("Failed to take item from queue", e);
      }

    }
  }
}
