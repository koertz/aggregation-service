package nl.zencode.restservice.common;

import java.util.List;
import java.util.Map;

/**
 * Response of ShippingAggregateService/aggregate.
 * @author Koert Zeilstra
 */
public class ShippingAggregateResponse {

  private Map<String, Double> pricing;
  private Map<String, String> track;
  private Map<String, List<String>> shipments;

  /**
   * @param pricing Pricing service response.
   * @param track Track service response.
   * @param shipments Shipments response.
   */
  public ShippingAggregateResponse(Map<String, Double> pricing, Map<String, String> track,
      Map<String, List<String>> shipments) {
    this.pricing = pricing;
    this.track = track;
    this.shipments = shipments;
  }

  /**
   * @return Pricing service response.
   */
  public Map<String, Double> getPricing() {
    return pricing;
  }

  /**
   * @return Track service response.
   */
  public Map<String, String> getTrack() {
    return track;
  }

  /**
   * @return Shipments response.
   */
  public Map<String, List<String>> getShipments() {
    return shipments;
  }
}
