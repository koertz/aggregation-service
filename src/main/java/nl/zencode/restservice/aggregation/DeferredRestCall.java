package nl.zencode.restservice.aggregation;

import java.util.concurrent.CompletableFuture;

/**
 * Keeps track if a deferred call to REST service.
 * @author Koert Zeilstra
 */
class DeferredRestCall<T> {

  private String parameterValue;
  private CompletableFuture<T> future;

  /**
   * @param parameterValue Parameter value for REST service - this is just a single value, not multiple comma-separated values. 
   */
  public DeferredRestCall(String parameterValue) {
    this.parameterValue = parameterValue;
    this.future = new CompletableFuture<>();
  }

  /**
   * @return  Parameter value for REST service - this is just a single value, not multiple comma-separated values. 
   */
  public String getParameterValue() {
    return parameterValue;
  }

  /**
   * @return Future result of service - call get() to retrieve result, which will be blocked until service is actually called.
   */
  public CompletableFuture getFuture() {
    return future;
  }

  /**
   * Complete the future result.
   * @param response Actual result of REST call.
   */
  public void complete(T response) {
    future.complete(response);
  }

}
