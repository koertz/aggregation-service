package nl.zencode.restservice.aggregation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.springframework.web.client.RestTemplate;

/**
 * Queue of deferred REST calls.
 * @author Koert Zeilstra
 */
class DeferredCallQueue<T> {

  private int maxQueueSize;
  private RestTemplate restTemplate;
  private String url;
  private Class<T> responseType;
  private Map<String, DeferredRestCall> calls = new HashMap<>();
  private Timer timer;

  /**
   * @param maxQueueSize Maximum queue size before calls in queue will be processed.
   * @param restTemplate REST template.
   * @param url URL to service.
   */
  public DeferredCallQueue(int maxQueueSize, RestTemplate restTemplate, String url) {
    this.maxQueueSize = maxQueueSize;
    this.restTemplate = restTemplate;
    this.url = url;
    this.responseType = responseType;
  }

  /**
   * Add REST call.
   * @param restCall REST call.
   */
  public void add(DeferredRestCall restCall) {
    synchronized (calls) {
      calls.put(restCall.getParameterValue(), restCall);
      // when this is the first in the queue, start a timer to make sure it is executed after 5 seconds.
      if (calls.size() == 1) {
        this.timer = new Timer();
        timer.schedule(new ExpirationTimer(), 5000);
      }
      if (calls.size() >= maxQueueSize) {
        execute();
      }
    }
  }

  public void execute() {
    synchronized (calls) {
      this.timer.cancel();
      List<String> parameters = new ArrayList<>(calls.keySet());
      final String combinedParameters = String.join(",", parameters);
      Map<String, T> combinedResponse = restTemplate.getForObject(url + combinedParameters, Map.class);
      for (String key : combinedResponse.keySet()) {
        calls.get(key).complete(combinedResponse.get(key));
      }
      calls.clear();
    }
  }

  /**
   * 
   */
  private class ExpirationTimer extends TimerTask {
    @Override
    public void run() {
      execute();
    }
  }
}
