package nl.zencode.restservice.aggregation;

import java.math.BigDecimal;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

/**
 * @author Koert Zeilstra
 */
@Component
@ApplicationScope
public class PricingClient {
  
  private BlockingQueue<DeferredRestCall<BigDecimal>> queue = new LinkedBlockingDeque<>();
  private ExecutorService executorService = Executors.newCachedThreadPool();
  
  @PostConstruct
  public void initialize() {
    executorService.execute(new QueueConsumer(queue));
  }

  /**
   * Collect GET calls to url until limit of maximum queue size is reached. If limit is reached, call service with combined
   * values and complete the returned future values.
   * @param parameterValue Single parameter value for service.
   * @param <T> Type of object in response map.
   * @return
   */
  public <T> Future<T> get(String parameterValue) {
    DeferredCallQueue<T> callQueue = null;
    DeferredRestCall<BigDecimal> restCall = new DeferredRestCall<>(parameterValue);
    try {
      queue.put(restCall);
    } catch (InterruptedException e) {
      throw new ServiceException("Failed to queue pricing service call with parameter: " + parameterValue, e);
    }

    return restCall.getFuture();
  }

  private static class QueueConsumer implements Runnable {
    private BlockingQueue<DeferredRestCall<BigDecimal>> queue;

    public QueueConsumer(
        BlockingQueue<DeferredRestCall<BigDecimal>> queue) {
      this.queue = queue;
    }

    @Override
    public void run() {
      try {
        DeferredRestCall<BigDecimal> deferredRestCall = queue.take();
      } catch (InterruptedException e) {
        throw new ServiceException("Failed to take item from queue", e);
      }
    }
  }

}
