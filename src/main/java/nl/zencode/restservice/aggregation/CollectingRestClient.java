package nl.zencode.restservice.aggregation;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.ApplicationScope;

/**
 * This is a client for a REST service that calls a service based on query parameter that may contain a list of values,
 * separated with a comma. It will first collect parameter values until a threshold - the service will then combine the
 * collected parameter values into one (comma delimited) call and return the retrieved results back to the caller.
 * 
 * 
 * @author Koert Zeilstra
 */
@Component
@ApplicationScope
public class CollectingRestClient {
  private static final int MAX_QUEUE_SIZE = 5;
  
  @Autowired private RestTemplate restTemplate;

  private Map<String, DeferredCallQueue> callQueues = new HashMap<>();
  
  @PostConstruct
  public void initialize() {
    
  }
  
  /**
   * Collect GET calls to url until limit of maximum queue size is reached. If limit is reached, call service with combined
   * values and complete the returned future values.
   * @param url Base URL of called service, including query parameter, for instance: http://localhost:8080/pricing?q=
   * @param parameterValue Single parameter value for service.
   * @param <T> Type of object in response map.
   * @return
   */
  public <T> Future<T> get(String url, String parameterValue) {
    DeferredCallQueue<T> callQueue = null;
    if (callQueues.containsKey(url)) {
      callQueue = callQueues.get(url);
    } else {
      callQueue = new DeferredCallQueue(MAX_QUEUE_SIZE, restTemplate, url);
      callQueues.put(url, callQueue);
    }
    DeferredRestCall<T> restCall = new DeferredRestCall(parameterValue);
    callQueue.add(restCall);
    
    return restCall.getFuture();
  }


}
