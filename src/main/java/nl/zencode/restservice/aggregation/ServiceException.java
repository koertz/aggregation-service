package nl.zencode.restservice.aggregation;

/**
 * @author Koert Zeilstra
 */
public class ServiceException extends RuntimeException {

  public ServiceException(String message, Throwable cause) {
    super(message, cause);
  }
}
