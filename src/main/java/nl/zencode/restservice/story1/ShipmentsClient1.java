package nl.zencode.restservice.story1;

import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.ApplicationScope;

/**
 * Client for shipments backing service.
 * @author Koert Zeilstra
 */
@Component
@ApplicationScope
public class ShipmentsClient1 {
  
  private static final String URL = "http://localhost:8080/shipments?q=";
  private static final Logger log = LoggerFactory.getLogger(ShipmentsClient1.class);
  
  @Autowired private RestTemplate restTemplate;
  private BackingServiceClient1<List<String>> backingServiceClient;

  @PostConstruct
  public void initialize() {
    backingServiceClient = new BackingServiceClient1<>(URL, restTemplate);
  }

  /**
   * Call backing shipments service.
   * @param parameterValue Parameter value for service.
   * @return Service response.
   */
  public Map<String, List<String>> get(String parameterValue) {
    return backingServiceClient.get(parameterValue);
  }

}
