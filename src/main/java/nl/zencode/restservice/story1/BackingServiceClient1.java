package nl.zencode.restservice.story1;

import java.math.BigDecimal;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * Generic client for backing service.
 * @author Koert Zeilstra
 */
public class BackingServiceClient1<T> {
  
  private static final Logger log = LoggerFactory.getLogger(BackingServiceClient1.class);

  private String url;
  private RestTemplate restTemplate;

  public BackingServiceClient1(String url, RestTemplate restTemplate) {
    this.url = url;
    this.restTemplate = restTemplate;
  }

  /**
   * Call backing pricing service.
   * @param parameterValue Parameter value for service.
   * @return Service response.
   */
  public Map<String, T> get(String parameterValue) {
    Map<String, T> response = null;
    if (parameterValue != null && !parameterValue.isEmpty()) {
      try {
        response = restTemplate.getForObject(url + parameterValue, Map.class);
      } catch (HttpServerErrorException serverException) {
        log.debug("Service failure: {}", serverException.getMessage());
        response = null;
      }
    }
    return response;
  }

}
