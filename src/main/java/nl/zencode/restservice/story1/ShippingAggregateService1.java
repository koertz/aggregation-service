package nl.zencode.restservice.story1;

import java.util.List;
import java.util.Map;
import nl.zencode.restservice.common.ShippingAggregateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Service that combines several other rest service into one aggregate service.
 * @author Koert Zeilstra
 */
@RestController
public class ShippingAggregateService1 {

  @Autowired RestTemplate restTemplate;
  @Autowired private PricingClient1 pricingClient;
  @Autowired private TrackClient1 trackClient;
  @Autowired private ShipmentsClient1 shipmentsClient;


  /**
   * REST service /aggregation - aggregates multiple calls into one response.
   * @param pricing Parameter values for pricing service.
   * @param track Parameter values for track service.
   * @param shipments Parameter values for shipments service.
   * @return Aggregated response.
   */
  @GetMapping("/aggregation1")
  public ShippingAggregateResponse aggregation(@RequestParam(value = "pricing", required = false) String pricing, 
      @RequestParam(value = "track", required = false) String track, 
      @RequestParam(value = "shipments", required = false) String shipments) {

    Map<String, Double> pricingResponse = pricingClient.get(pricing);
    Map<String, String> trackResponse = trackClient.get(track);
    Map<String, List<String>> shipmentsResponse = shipmentsClient.get(shipments);
    
    return new ShippingAggregateResponse(pricingResponse, trackResponse, shipmentsResponse);
  }

}
