package nl.zencode.restservice.story1;

import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.ApplicationScope;

/**
 * Client for pricing backing service.
 * @author Koert Zeilstra
 */
@Component
@ApplicationScope
public class PricingClient1 {
  
  private static final String URL = "http://localhost:8080/pricing?q=";

  @Autowired private RestTemplate restTemplate;
  private BackingServiceClient1<Double> backingServiceClient;
  
  @PostConstruct
  public void initialize() {
    backingServiceClient = new BackingServiceClient1<>(URL, restTemplate);
  }

  /**
   * Call backing pricing service.
   * @param parameterValue Parameter value for service.
   * @return Service response.
   */
  public Map<String, Double> get(String parameterValue) {
    return backingServiceClient.get(parameterValue);
  }

}
