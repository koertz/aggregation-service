package nl.zencode.restservice.story1;

import java.util.Map;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.ApplicationScope;

/**
 * Client for track backing service.
 * @author Koert Zeilstra
 */
@Component
@ApplicationScope
public class TrackClient1 {
  
  private static final String URL = "http://localhost:8080/track?q=";
  private static final Logger log = LoggerFactory.getLogger(TrackClient1.class);
  
  @Autowired private RestTemplate restTemplate;
  private BackingServiceClient1<String> backingServiceClient;

  @PostConstruct
  public void initialize() {
    backingServiceClient = new BackingServiceClient1<>(URL, restTemplate);
  }

  /**
   * Call backing track service.
   * @param parameterValue Parameter value for service.
   * @return Service response.
   */
  public Map<String, String> get(String parameterValue) {
    return backingServiceClient.get(parameterValue);
  }

}
