package nl.zencode.restservice.stub;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Stub implementation of shipments, track, and pricing services.
 * @author Koert Zeilstra
 */
@RestController
public class StubService {

  private static final String[] TRACK_VALUES = {"NEW", "COLLECTING"};
  private static final String[][] SHIPMENTS_VALUES = {new String[] {"box", "box", "pallet"}, new String[] {"envelope"}};
  
  @GetMapping("/shipments")
  public Map<String, List<String>> shipments(@RequestParam(value = "q") String query) {
    Random random = new Random(42);
    determineFailure(random);
    Map<String, List<String>> response = new HashMap<>();
    for (String queryValue : query.split(",")) {
      response.put(queryValue, Arrays.asList(SHIPMENTS_VALUES[Math.abs(random.nextInt()) % SHIPMENTS_VALUES.length]));
    }
    return response;
  }
  
  @GetMapping("/track")
  public Map<String, String> track(@RequestParam(value = "q") String query) {
    Random random = new Random(42);
    determineFailure(random);
    Map<String, String> response = new HashMap<>();
    for (String queryValue : query.split(",")) {
      response.put(queryValue, TRACK_VALUES[Math.abs(random.nextInt()) % TRACK_VALUES.length]);
    }
    return response;
  }
  
  @GetMapping("/pricing")
  public Map<String, BigDecimal> pricing(@RequestParam(value = "q") String query) {
    Random random = new Random(42);
    determineFailure(random);
    Map<String, BigDecimal> response = new HashMap<>();
    for (String queryValue : query.split(",")) {
      response.put(queryValue, new BigDecimal(random.nextDouble() * 10, new MathContext(2)));
    }
    return response;
  }

  /**
   * Randomly sleep between 0 and 1000 milliseconds and possibly cause a failure.
   */
  private void determineFailure(Random random) {
    try {
      Thread.sleep(random.nextInt(1000));
    } catch (InterruptedException e) {
    }
    if (random.nextInt(100) < 25) {
      throw new ServiceUnavailableException("service unavailable");
    }
  }
  
}
