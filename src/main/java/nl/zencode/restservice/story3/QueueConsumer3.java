package nl.zencode.restservice.story3;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import nl.zencode.restservice.aggregation.ServiceException;
import nl.zencode.restservice.common.BackingServiceClient;

/**
 * Queue for service calls to backing services.
 * This runs continuously to handle calls asynchronously.
 * It collects single call parameters until it has received AggregationConfig.QUEUE_MAX_SIZE number of values; then it will combine
 * these values into a comma separated list and calls the backing service with that list of values.
 * @param <T> Type of response from backing service.
 * @author Koert Zeilstra
 */
class QueueConsumer3<T> implements Runnable {

  private ExecutorService executorService;
  private BlockingQueue<DeferredRestCall3<T>> queue;
  private BackingServiceClient<T> backingServiceClient;

  private boolean running = true;
  private List<DeferredRestCall3<T>> receivedItems = new ArrayList<>();
  private long earliestExpirationTime;
  private long pollTimeout = Long.MAX_VALUE;
  
  public QueueConsumer3(ExecutorService executorService, BlockingQueue<DeferredRestCall3<T>> queue,
      BackingServiceClient<T> backingServiceClient) {
    this.executorService = executorService;
    this.queue = queue;
    this.backingServiceClient = backingServiceClient;
  }

  @Override
  public void run() {
    while (running) {
      try {
        DeferredRestCall3<T> deferredRestCall = queue.poll(pollTimeout, TimeUnit.MILLISECONDS);
        if (deferredRestCall == null) {
          processItemsInQueue();
        } else {
          receivedItems.add(deferredRestCall);
          if (receivedItems.size() == 1) {
            earliestExpirationTime = deferredRestCall.getExpirationTime();
          } else {
            earliestExpirationTime = Math.min(earliestExpirationTime, deferredRestCall.getExpirationTime());
          }

          if (receivedItems.size() >= AggregationConfig.QUEUE_MAX_SIZE) {
            processItemsInQueue();
          }
          pollTimeout = Math.max(earliestExpirationTime - System.currentTimeMillis(), 0);
        }
      } catch (InterruptedException e) {
        throw new ServiceException("Failed to take item from queue", e);
      }

    }
  }

  /**
   * Call backing service for all items in queue, reset receivedItems and pollTimeout.
   */
  private void processItemsInQueue() {
    executorService.execute(new ReceivedItemsProcessor<>(backingServiceClient, receivedItems));
    receivedItems = new ArrayList<>();
    pollTimeout = Long.MAX_VALUE;
  }

  /**
   * Calls the backing service asynchronously.
   * @param <T> Type of response from backing service.
   */
  private static class ReceivedItemsProcessor<T> implements Runnable {

    private BackingServiceClient<T> backingServiceClient;
    private List<DeferredRestCall3<T>> receivedItems;

    public ReceivedItemsProcessor(BackingServiceClient<T> backingServiceClient, List<DeferredRestCall3<T>> receivedItems) {
      this.backingServiceClient = backingServiceClient;
      this.receivedItems = receivedItems;
    }

    @Override
    public void run() {
      String combinedParameters = receivedItems.stream().map(DeferredRestCall3::getParameterValue)
          .collect(Collectors.joining(","));
      Map<String, T> combinedResponse = backingServiceClient.get(combinedParameters);
      if (combinedResponse == null) {
        for (DeferredRestCall3<T> receivedItem : receivedItems) {
          receivedItem.complete(null);
        }
      } else {
        for (DeferredRestCall3<T> receivedItem : receivedItems) {
          receivedItem.complete(combinedResponse.get(receivedItem.getParameterValue()));
        }
      }
    }
  }
}
