package nl.zencode.restservice.story3;

import java.util.concurrent.CompletableFuture;

/**
 * Keeps track if a deferred call to REST service.
 * @author Koert Zeilstra
 */
class DeferredRestCall3<T> {

  private String parameterValue;
  private CompletableFuture<T> future;
  private long expirationTime;

  /**
   * @param parameterValue Parameter value for REST service - this is just a single value, not multiple comma-separated values. 
   * @param expirationTime Latest time that this call must be started/invoked.
   */
  public DeferredRestCall3(String parameterValue, long expirationTime) {
    this.parameterValue = parameterValue;
    this.expirationTime = expirationTime;
    this.future = new CompletableFuture<>();
  }

  /**
   * @return  Parameter value for REST service - this is just a single value, not multiple comma-separated values. 
   */
  public String getParameterValue() {
    return parameterValue;
  }

  /**
   * @return Future result of service - call get() to retrieve result, which will be blocked until service is actually called.
   */
  public CompletableFuture<T> getFuture() {
    return future;
  }

  /**
   * @return Latest time that this call must be started/invoked.
   */
  public long getExpirationTime() {
    return expirationTime;
  }

  /**
   * Complete the future result.
   * @param response Actual result of REST call.
   */
  public void complete(T response) {
    future.complete(response);
  }

}
