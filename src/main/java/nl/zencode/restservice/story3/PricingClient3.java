package nl.zencode.restservice.story3;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import javax.annotation.PostConstruct;
import nl.zencode.restservice.aggregation.ServiceException;
import nl.zencode.restservice.common.BackingServiceClient;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.ApplicationScope;

/**
 * Client for pricing backing service.
 * @author Koert Zeilstra
 */
@Component
@ApplicationScope
public class PricingClient3 {
  
  private static final String URL = "http://localhost:8080/pricing?q=";

  private RestTemplate restTemplate;
  private ExecutorService executorService = Executors.newCachedThreadPool();
  private BlockingQueue<DeferredRestCall3<Double>> queue = new LinkedBlockingDeque<>();
  private BackingServiceClient<Double> backingServiceClient;

  public PricingClient3(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @PostConstruct
  public void initialize() {
    backingServiceClient = new BackingServiceClient<>(URL, restTemplate);
    executorService.execute(new QueueConsumer3<>(executorService, queue, backingServiceClient));
  }


  /**
   * Call backing pricing service.
   * @param parameterValue Parameter value for service, may contain multiple comma separated values.
   * @return Service response.
   */
  public Map<String, Future<Double>> get(String parameterValue) {
    Map<String, Future<Double>> futureResponse = new HashMap<>();
    if (parameterValue != null && !parameterValue.isEmpty()) {
      for (String value : parameterValue.split(",")) {
        try {
          long expirationTime = System.currentTimeMillis() + AggregationConfig.QUEUE_TIMEOUT; // Latest time that this item must be invoked
          DeferredRestCall3<Double> deferredCall = new DeferredRestCall3<>(value, expirationTime);
          queue.put(deferredCall);
          futureResponse.put(value, deferredCall.getFuture());
        } catch (InterruptedException e) {
          throw new ServiceException("Failed to insert into queue", e);
        }
      }
    }

    return futureResponse;
  }
}
