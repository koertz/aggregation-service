package nl.zencode.restservice.story3;

/**
 * Global configuration contants.
 * @author Koert Zeilstra
 */
public final class AggregationConfig {
  public static final long QUEUE_TIMEOUT = 5000; // Maximum age of queue item in milliseconds.
  public static final long QUEUE_MAX_SIZE = 5; // Maximum number of items in queue.
  

}
